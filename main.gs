const tempIndex = HtmlService.createTemplateFromFile('index');
const tempInput = HtmlService.createTemplateFromFile('input');
const tempEdit = HtmlService.createTemplateFromFile('edit');

function pageIndex() {
  const indexOutput = tempIndex.evaluate();
  indexOutput
  .setTitle('賃貸物件ふぁぼ一覧')
  .addMetaTag('viewport', 'width=device-width, initial-scale=1');
  
  return indexOutput;
}

function pageInput() {
  const inputOutput = tempInput.evaluate();
  inputOutput
  .setTitle('ふぁぼ物件追加')
  .addMetaTag('viewport', 'width=device-width, initial-scale=1');
  
  return inputOutput;
}

function pageEdit() {
  const editOutput = tempEdit.evaluate();
  editOutput
  .setTitle('ふぁぼ物件編集')
  .addMetaTag('viewport', 'width=device-width, initial-scale=1');
  
  return editOutput;
}


//シートの取得
const ssId = PropertiesService.getScriptProperties().getProperty('spreadSheetId');
const listSheet = SpreadsheetApp.openById(ssId).getSheetByName('list');


//listシートのデータ群
var dataAry = listSheet.getDataRange().getValues();
//項目（ヘッダ）行
var headerAry = dataAry[0]; //一次元
//データ行
var bodyAry = dataAry.slice(1); //二次元


function doGet(e) {
    if (e.parameter.mode == 'edit') {
      //編集画面
      //項目名（一次元）
      tempEdit.itemName = headerAry;
      //対象データ（一次元）
      var targetIdx = matchId(e.parameter.id);
      bodyAry = listSheet.getDataRange().getValues().slice(1);
      tempEdit.res = bodyAry[targetIdx];
      
      return pageEdit();
      
    } else if (e.parameter.mode == 'input') {
      //追加画面
      return pageInput();
    
    } else {
      // e.parameter.mode == 'index'
      //一覧画面
      //項目名（一次元）
      tempIndex.itemName = headerAry;    
      //データ（二次元）
      tempIndex.res = listSheet.getDataRange().getValues().slice(1);
      
      return pageIndex();
    }      
}


function doPost(e) {
  tempIndex.itemName = headerAry;  
  
  if (e.parameter.mode == 'edit') {
    //編集時
    var newAry = [];
    newAry.push(getParamet(e));

    //編集対象IDの行に上書き
    var editIdx = matchId(newAry[0][2]);
    if (editIdx !== -1) {
      listSheet.getRange(editIdx+2,1,1,newAry[0].length).setValues(newAry);       
    }
    
    console.log("editing completed!");
    tempIndex.res = listSheet.getDataRange().getValues().slice(1);
    
    return pageIndex();
    
  } else if (e.parameter.mode == 'input') {
    //追加時
    getHtml(e.parameter.inputUrl);
    
    console.log("adding completed!");
    tempIndex.res = listSheet.getDataRange().getValues().slice(1);
    
    return pageIndex();
  }
}


//項目が増減したら編集
function getParamet(e) {
  var propAry = [];
  
  propAry.push(e.parameter.d_item0); //A列/建物名
  propAry.push(e.parameter.d_item1); //B列/URL
  propAry.push(e.parameter.d_item2); //C列/ID
  propAry.push(e.parameter.d_item3); //D列
  propAry.push(e.parameter.d_item4); //E列
  propAry.push(e.parameter.d_item5); //F列
  propAry.push(e.parameter.d_item6); //G列
  propAry.push(e.parameter.d_item7); //H列
  propAry.push(e.parameter.d_item8); //I列
  propAry.push(e.parameter.d_item9); //J列/外観
  propAry.push(e.parameter.d_item10); //K列/間取り図
  propAry.push(e.parameter.d_item11); //L列/所在地
  propAry.push(e.parameter.d_item12); //M列
  propAry.push(e.parameter.d_item13); //N列/通勤時間
  propAry.push(e.parameter.d_item14); //O列
  propAry.push(e.parameter.d_item15); //P列
  propAry.push(e.parameter.d_item16); //Q列
  propAry.push(e.parameter.d_item17); //R列
  propAry.push(e.parameter.d_item18); //S列
  propAry.push(e.parameter.d_item19); //T列
  propAry.push(e.parameter.d_item20); //U列/設備
  propAry.push(e.parameter.d_item21); //V列
  propAry.push(e.parameter.d_item22); //W列
  propAry.push(e.parameter.d_item23); //X列
  propAry.push(e.parameter.d_item24); //Y列
  
  return propAry;
}


//ID配列から編集対象IDと一致するインデックスを検索
function matchId(id) {
  bodyAry = listSheet.getDataRange().getValues().slice(1);
  var idAry = [];
  for (var i = 0; i < bodyAry.length; i++) {
    idAry.push(bodyAry[i][2].toString()); //Dateをformatした時点ではStringだが、setValueすると数字扱いされるためindexOfするにはString化が必要
  }
  console.log("target ID = " + id + " : index = " + idAry.indexOf(id));

  return idAry.indexOf(id);
}
