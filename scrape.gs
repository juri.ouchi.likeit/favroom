function getHtml(url){
//  var url = 'https://www.homes.co.jp/chintai/***/';
  
  try{
    var fetch = UrlFetchApp.fetch(url);
    var html = fetch.getContentText();
    
    var ppId = getPropertyId(new Date());  
    var bkId = PropertiesService.getScriptProperties().getProperty('htmlFolderId');
    var bkFolder = DriveApp.getFolderById(bkId);
    var bkHtml = bkFolder.createFile(ppId + '.html', html, MimeType.HTML);
    console.log('ppID => ' + ppId + '\ntarget_URL => ' + url + '\nbkHTML_ID => ' + bkHtml.getId());
    
    if(url.indexOf('suumo.jp/chintai/jnc') !== -1) {
      collectSuumoData(ppId, url, html);
    } else if(url.indexOf('homes') !== -1) {
      collectHomesData(ppId, url, html);
    } else {
      console.log('未対応のアドレスです。。');
    }
    
  } catch(e){
    console.log('スクレイプできなかったよ：' + e);
  }
}


/*
 * HTMLからデータ収集、整形【suumo】
 */
function collectSuumoData(ppId, url, html){
  //書き込み用配列
  var parsedData = [];
  
  //建物名
  var fromText = '<h1 class="section_h1-header-title">';
  var toText = '</h1>';
  var title = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(title);
  
  //URL
  parsedData.push(url);
    
  //ID
  parsedData.push(ppId);
  
  //家賃
  fromText = '<span class="property_view_note-emphasis">';
  toText = '</span>';
  var yachin = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(yachin);
  
  //管理費
  fromText = '共益費:&nbsp;';
  toText = '</span>';
  var kanrihi = getStr(html, fromText, toText);
  parsedData.push(kanrihi);
  
  //敷金
  fromText = '敷金:&nbsp;';
  toText = '</span>';
  var shikikin = getStr(html, fromText, toText);
  parsedData.push(shikikin);
  
  //礼金
  fromText = '礼金:&nbsp;';
  toText = '</span>';
  var reikin = getStr(html, fromText, toText);
  parsedData.push(reikin);
  
  //保証金
  fromText = '保証金:&nbsp;';
  toText = '</span>';
  var hoshokin = getStr(html, fromText, toText);
  parsedData.push(hoshokin);
  
  //敷引・償却
  fromText = '償却:&nbsp;';
  toText = '</span>';
  var expence = getStr(html, fromText, toText);
  parsedData.push(expence);

  //外観写真
  fromText = '<img data-src="';
  toText = '" alt="建物外観';
  var gaikanUrl = getStr(html, fromText, toText).replace('t.jpg', 'o.jpg');
  console.log(gaikanUrl);
  var gaikanId = savePhoto(gaikanUrl);
  parsedData.push(gaikanId);
  sleep();
  
  //間取図
  fromText = '<img data-src="';
  toText = '" alt="間取';  
  var backwards = Parser
  .data(html)
  .from(fromText)
  .to(toText)
  .setDirection()
  .build();
  var madoriUrl = backwards.replace('t.jpg', 'o.jpg');
  console.log(madoriUrl);
  var madoriId = savePhoto(madoriUrl);
  parsedData.push(madoriId);
  
  //所在地
  fromText = '所在地</th>';
  toText = '</td>';
  var address = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(convert2half(address));
  
  //駅徒歩
  fromText = '駅徒歩</th>';
  toText = '</div>';
  var toho = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(convert2half(toho));
  
  //通勤時間
  var sttST = toho.match(/(\/| ).+駅/)[0].replace(/(\/| |駅)/g, '');
  console.log(sttST);
  var times = getTrainTime(sttST);
  parsedData.push(times + '分');
  
  //間取り
  fromText = '間取り</th>';
  toText = '</td>';
  var rldk = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(rldk);
  
  //専有面積
  fromText = '専有面積</th>';
  toText = '<sup>';
  var menseki = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '').replace('m', '㎡');
  parsedData.push(menseki);
  
  //構造
  fromText = '構造</th>';
  toText = '</td>';
  var material = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(material);
  
  //築年数
  fromText = '築年数</th>';
  toText = '</td>';
  var age = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(age);
  
  //階
  fromText = '階建</th>';
  toText = '</td>';
  var floor = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(floor);
  
  //向き
  fromText = '向き</th>';
  toText = '</td>';
  var window = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(window);
  
  //設備
  fromText = '<div class="bgc-wht ol-g">';
  toText = '</li>';
  var equip = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');  
  parsedData.push(equip);
  
  
  fromText = '保証会社</th>';
  toText = '</table>';
  var moneyData = getStr(html, fromText, toText);
  fromText = '<li>';
  toText = '</li>';
  var moneyAry = getAry(moneyData, fromText, toText);
  
  //保証会社
  parsedData.push(convert2half(moneyAry[0].replace(/(<.+>|\s)/gm, '')));
  
  //初期費用
  var starter = "";
  for (var i = 1; i < moneyAry.length; i++) {
     starter += '　' + moneyAry[i];
  }
  parsedData.push(convert2half(starter.replace(/(<.+>|\s)/gm, '')));
  
  console.log("scraping completed!");

  //シートに記入
  listSheet.appendRow(parsedData); // 引数は1次元配列、null不可
}


/*
 * HTMLからデータ収集、整形【Home's】
 */
function collectHomesData(ppId, url, html){
  //書き込み用配列
  var parsedData = [];
  
  //建物名
  var fromText = '<span class="bukkenName" id="chk-bkh-name">';
  var toText = '</span>';
  var title = getStr(html, fromText, toText);
  parsedData.push(title); 
    
  //URL
  parsedData.push(url);   

  //ID
  parsedData.push(ppId);
  
  //家賃
  fromText = '<dd id="chk-bkc-moneyroom"><span class="num"><span>';
  toText = '</span>'; 
  var yachin = getStr(html, fromText, toText);
  parsedData.push(yachin + '万円');
  
  //管理費
  fromText = '</span>万円</span> (';
  toText = ')</dd>';
  var kanrihi = getStr(html, fromText, toText).replace(/,|\s/, '');
  parsedData.push(kanrihi);
  
  
  fromText = '<dd id="chk-bkc-moneyshikirei">';
  toText = '</dd>';
  var shikirei = getStr(html, fromText, toText);
  //敷金
  parsedData.push(shikirei.match(/.+\//)[0].replace(/\s*\//, ''));
  //礼金
  parsedData.push(shikirei.match(/\/.+/)[0].replace(/\/\s*/, ''));
  
  
  fromText = '<dd id="chk-bkc-moneyhoshoukyaku">';
  toText = '</dd>';
  var hoshoukyaku = getStr(html, fromText, toText);
  //保証金
  parsedData.push(hoshoukyaku.match(/.+\//)[0].replace(/\s*\//, ''));
  //敷引・償却
  parsedData.push(hoshoukyaku.match(/\/.+/)[0].replace(/\/\s*/, ''));

  
  //外観写真
  fromText = 'rev="gaikan">';
  toText = '<span';
  var gaikanElem = getStr(html, fromText, toText);
  var gaikanUrl = gaikanElem.match(/(http%3A%2F%2F).+\.(jpg|gif|png)/)[0].replace('%3A', ':').replace(/%2F/g, '/');
  Logger.log(gaikanUrl);
  var gaikanId = savePhoto(gaikanUrl);
  parsedData.push(gaikanId);
  sleep();
  
  //間取図
  fromText = 'rev="madori">';
  toText = '<span';
  var madoriElem = getStr(html, fromText, toText);
  var madoriUrl = madoriElem.match(/(http%3A%2F%2F).+\.(jpg|gif|png)/)[0].replace('%3A', ':').replace(/%2F/g, '/');
  Logger.log(madoriUrl);
  var madoriId = savePhoto(madoriUrl);
  parsedData.push(madoriId);
  
  //所在地
  fromText = '<dd id="chk-bkc-fulladdress">';
  toText = '<p>';
  var address = getStr(html, fromText, toText).replace(/\s/gm, '');
  parsedData.push(address);
  
  //駅徒歩
  fromText = '<dd id="chk-bkc-fulltraffic">';
  toText = '</p>';
  var toho = getStr(html, fromText, toText).replace(/\s*<p>/m, '');
  parsedData.push(toho);
  
  //通勤時間
  var sttST = toho.match(/(\/| ).+駅/)[0].replace(/(\/| |駅)/g, '');
  console.log(sttST);
  var times = getTrainTime(sttST);
  parsedData.push(times + '分');
  
  //間取り
  fromText = '<dd id="chk-bkc-marodi">';
  toText = '</dd>';
  var rldk = getStr(html, fromText, toText).replace(/\s/gm, '').replace(/\(.+\)/, '');
  parsedData.push(rldk);
  
  //専有面積
  fromText = '<dd id="chk-bkc-housearea">';
  toText = '</dd>';
  var menseki = getStr(html, fromText, toText).replace(/\s/gm, '').replace('m&sup2;', '㎡');
  parsedData.push(menseki);
  
  //構造
  fromText = '<td id="chk-bkd-housekouzou">';
  toText = '</td>';
  var material = getStr(html, fromText, toText).replace(/\s/gm, '');
  parsedData.push(material);
  
  //築年数
  fromText = '<dd id="chk-bkc-kenchikudate">';
  toText = '</dd>';
  var age = getStr(html, fromText, toText).match(/\(\s.+\s\)/)[0].replace(/\(|\s|\)/g,'');
  parsedData.push(age);
  
  //階
  fromText = '<td id="chk-bkd-housekai">';
  toText = '</td>';
  var floor = getStr(html, fromText, toText).replace(/\s/gm, '');
  parsedData.push(floor);

  //向き
  fromText = '<dd id="chk-bkc-windowangle">';
  toText = '</dd>';
  var window = getStr(html, fromText, toText);
  parsedData.push(window);
  
  //特徴・設備
  fromText = '<td id="chk-bkf-setsubi';
  toText = '</td>';
  var equipAry = getAry(html, fromText, toText);
  var equip = equipAry[0].replace(/(^.+>|\s)/gm, '');
  for (var i = 1; i < equipAry.length; i++) {
     equip += '。' + equipAry[i].replace(/(^.+>|\s)/gm, '');
  }
  parsedData.push(equip);
    
  //保証会社
  fromText = '<td id="chk-bkd-guaranteecom">';
  toText = '</td>';
  var hoCom = getStr(html, fromText, toText).replace(/(<.+>|\s)/gm, '');
  parsedData.push(hoCom);

  //初期費用
  fromText = '<td colspan="3" id="chk-bkd-moneyother">';
  toText = '</td>';
  var starter = '';
  if (html.indexOf('id="chk-bkd-moneyother"') !== -1){
    starter = getStr(html, fromText, toText);
  }
  parsedData.push(starter);
 
  //更新料
  fromText = '<td id="chk-bkd-moneykoushin">';
  toText = '</td>';
  var reCon = '';
  if (html.indexOf('id="chk-bkd-moneykoushin"') !== -1){
    reCon = getStr(html, fromText, toText);
  }
  parsedData.push(reCon);
  
  //備考  
  fromText = '<td id="chk-bkf-biko">';
  toText = '</td>';
  var biko = '';
  if (html.indexOf('id="chk-bkf-biko"') !== -1){
    biko = getStr(html, fromText, toText).replace(/\s/gm, '');
  }
  parsedData.push(biko);
  
  console.log("scraping completed!");

  //シートに記入
  listSheet.appendRow(parsedData); // 引数は1次元配列、null不可
}


/**
* 最初にヒットした文字列を抽出する
*
* @param {string} html パース対象データ
* @param {string} fromText 抽出したい箇所直前の文字列
* @param {string} toText 抽出したい箇所直後の文字列
* @return {string} parsedStr 抽出した文字列データ
*/ 
function getStr(html, fromText, toText){
  var parsedStr = Parser
  .data(html)
  .from(fromText)
  .to(toText)
  .build();
  
  return parsedStr;
}


/**
* ヒットした文字列をすべて抽出する
*
* @param {string} html パース対象データ
* @param {string} fromText 抽出したい箇所直前の文字列
* @param {string} toText 抽出したい箇所直後の文字列
* @return {array} parsedAry 抽出した文字列データを詰めた配列
*/ 
function getAry(html, fromText, toText){
  var parsedAry = Parser
  .data(html)
  .from(fromText)
  .to(toText)
  .iterate();
  
  return parsedAry;
}


//全角英数字とカンマを半角に直す
function convert2half(str){
  var ans = str.replace('，', ',').replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
    return String.fromCharCode(s.charCodeAt(0) - 65248);
  });
  
  return ans;
}