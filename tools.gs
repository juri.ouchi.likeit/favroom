/*
 * バックアップHTMLからリトライ
 */
function retryParse() {
  //以下3要素を変更
  var ppId = "***"; //bkファイル名
  var url = 'https://suumo.jp/chintai/***/'; //対象URL
  var FILE_ID = "***"; //bkファイルID
  
  var file = DriveApp.getFileById(FILE_ID);
  var html = file.getBlob().getDataAsString();
  
  if(url.indexOf('suumo.jp/chintai/jnc') !== -1) {
    collectSuumoData(ppId, url, html);
  } else if(url.indexOf('homes') !== -1) {
    collectHomesData(ppId, url, html);
  } else {
    console.log('未対応のアドレスです。。');
  }
}


/*
 * スクレイプ日時をIDとして記録
 */
function getPropertyId(date) {
	var format = 'YYYYMMDDhhmmss'
    
    // 現在日時を0埋めしてIDを生成
    format = format.replace(/YYYY/g, date.getFullYear());
    format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
    format = format.replace(/DD/g, ('0' + date.getDate()).slice(-2));
    format = format.replace(/hh/g, ('0' + date.getHours()).slice(-2));
    format = format.replace(/mm/g, ('0' + date.getMinutes()).slice(-2));
    format = format.replace(/ss/g, ('0' + date.getSeconds()).slice(-2));
    
  return format;
}


/*
 * APIを利用し、最寄り駅から東京駅までの所要時間を算出
 * https://api.trip2.jp/ex/tokyo/v1.0/json?src=出発駅名&dst=到着駅名&key=キー
 */
function getTrainTime(sttST) {
//  var sttST = "上本郷";
  var endST = "東京";
  var callUrl = 'https://api.trip2.jp/ex/tokyo/v1.0/json?src=' + sttST +'&dst=' + endST +'&key=58254654';
   
  var needTime = 0;
  
  try {
    var response = UrlFetchApp.fetch(callUrl).getContentText('UTF-8');
    var json = JSON.parse(response);  
    
    for (var i = 0; i < json["ways"].length; i++) {
      for( var prop in json["ways"][i] ) {
        if (prop === "min"){
          needTime += json["ways"][i][prop];
        }
      }
    }

    Logger.log(sttST + '駅　→　' + endST + '駅：所要時間　' + needTime + '分');
    return needTime;

  } catch (e) {
    console.log('検索範囲外の駅みたい。。：' + e);
    return 0;
  }
}


/*
 * 先方負荷軽減のため、処理を5秒間待機
 */
function sleep() {
  Utilities.sleep(1 * 5000); // 5000ミリ秒＝5秒
}


/*
 * 画像URLからDLしてドライブ保存し、画像IDをリストアップ
 */
function savePhoto(photoUrl){
//  var url = "https://sonicwire.com/images/sp/cv/mikuv4xb_img1.jpg";   
  
  // urlからファイルを受け取る
  var response = UrlFetchApp.fetch(photoUrl);
  console.log("Download: " + url + "\n => " + folder.getName());
 
  // 保存するファイル名をセット
  var fileName = photoUrl.split('/').pop(); // 最後の"/"以降をファイル名にする
  var fileBlob = response.getBlob();
  fileBlob.setName(fileName);
  
  // GoogleドライブURL欄記載の保存先フォルダーIDを入力
  var folderId = PropertiesService.getScriptProperties().getProperty('photoFolderId');
  var folder = DriveApp.getFolderById(folderId);
  
  // ファイル名がかぶっていたら、前のものは削除する
  // (Google Driveの場合、名前がかぶっていても上書き対象にはならない)
  var itr = folder.getFilesByName(fileName);
  if( itr.hasNext() ) {
    folder.removeFile(itr.next());
  }

  // 新規ファイルとして保存
  var newFile = folder.createFile(fileBlob);
  
  return newFile.getId(); // この画像IDをスプシに保存して、IDから画像呼び出して表示
}